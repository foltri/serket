import cv2
import morph_segmentation
import numpy as np
from scipy.spatial import distance as dist


def get_malacok(img):
        segmented_img = morph_segmentation.segmentation(img)
        cv2.imshow('segmented', segmented_img)
        cnts = __get_malac_cnts(segmented_img)
        si = cv2.cvtColor(segmented_img, cv2.COLOR_GRAY2BGR)
        return __get_malac_head_butts(si, cnts)


def __get_malac_cnts(segmented_img):
    cnt_img, cnts, hierarchy = cv2.findContours(segmented_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    # todo ez nem fog kelleni
    # filter out small cnts
    malac_cnts = []
    for cnt in cnts:
        if cnt.shape[0] > 20:
            malac_cnts.append(cnt)

    return malac_cnts


def __get_malac_head_butts(segmented_image, cnts):

    malacok = []
    for cnt in cnts:
        rect = cv2.minAreaRect(cnt)

        box = cv2.boxPoints(rect)
        box = np.int0(box)

        # sort box points into 4-4 points on the two sides of a malac rect for masking
        A = box[0]
        if dist.euclidean(box[0], box[1]) < dist.euclidean(box[0], box[3]):
            B = box[1]
            C = box[2]
            D = box[3]
        else:
            B = box[3]
            C = box[2]
            D = box[1]

        #print(dist.euclidean(box[0], box[1]))

        AD = (A + D) / 2
        BC = (B + C) / 2

        # get average color of masked parts
        avg1 = __get_rect_avg(segmented_image, np.uint0([A, B, BC, AD]))
        avg2 = __get_rect_avg(segmented_image, np.uint0([C, D, AD, BC]))

        if avg1 < avg2:
            head = tuple(np.uint0((A + B) / 2))
            butt = tuple(np.uint0((C + D) / 2))
        else:
            head = tuple(np.uint0((C + D) / 2))
            butt = tuple(np.uint0((A + B) / 2))

        # filter out positions that are out of image
        problem = [i > segmented_image.shape[1] for i in head + butt]
        if True not in problem:
            malacok.append({'head': head, 'butt': butt, 'box': box})


    return malacok


def __get_rect_avg(img, points):
    mask = np.zeros_like(img)
    cv2.fillConvexPoly(mask, points, [255, 255, 255])
    masked = cv2.bitwise_and(img, mask)
    avg = np.average(masked)
    return avg


if __name__ == "__main__":
    cap = cv2.VideoCapture(2)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    # morph_segmentation.create_trackbars()
    while True:
        img = cap.read()[1]
        roi = img[350:990, 498:1410]
        segmented_img = morph_segmentation.segmentation(roi)
        cnts = __get_malac_cnts(segmented_img)
        si = cv2.cvtColor(segmented_img, cv2.COLOR_GRAY2BGR)
        __get_malac_head_butts(si, cnts)
        cv2.waitKey(1)