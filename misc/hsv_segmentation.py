import cv2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib import colors


def main():
    img = cv2.imread("imgs/my_photo-3.jpg")
    roi = img[215:954, 458:1433]

    rgb = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
    hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV)

    while True:
        c = get_trackbar_pos()
        color1 = (c[0], c[2], c[4])
        color2 = (c[1], c[3], c[5])

        mask = cv2.inRange(hsv, color1, color2)
        result = cv2.bitwise_and(rgb, rgb, mask=mask)

        cv2.imshow("image", result)
        cv2.waitKey(1)

        # plt.subplot(1, 2, 1)
        # plt.imshow(mask, cmap="gray")
        # plt.subplot(1, 2, 2)
        # plt.imshow(result)
        # plt.show()


def get_trackbar_pos():
    # get current positions of four trackbars
    Hmin = cv2.getTrackbarPos('Hmin', 'settings')
    Hmax = cv2.getTrackbarPos('Hmax', 'settings')
    Smin = cv2.getTrackbarPos('Smin', 'settings')
    Smax = cv2.getTrackbarPos('Smax', 'settings')
    Vmin = cv2.getTrackbarPos('Vmin', 'settings')
    Vmax = cv2.getTrackbarPos('Vmax', 'settings')

    return Hmin, Hmax, Smin, Smax, Vmin, Vmax


def create_trackbars():

    def nothing(x):
        pass

    cv2.namedWindow("settings")

    # create trackbars for color change
    cv2.createTrackbar('Hmin', 'settings', 0, 255, nothing)
    cv2.createTrackbar('Hmax', 'settings', 19, 255, nothing)
    cv2.createTrackbar('Smin', 'settings', 42, 255, nothing)
    cv2.createTrackbar('Smax', 'settings', 127, 255, nothing)
    cv2.createTrackbar('Vmin', 'settings', 153, 255, nothing)
    cv2.createTrackbar('Vmax', 'settings', 255, 255, nothing)


def choose_color_range():
    img = cv2.imread("imgs/my_photo-1.jpg")
    roi = img[215:954, 458:1433]

    rgb = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
    hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV)

    plot_hsv_img(rgb)




def plot_hsv_img(rgb):
    pixel_colors = rgb.reshape((np.shape(rgb)[0] * np.shape(rgb)[1], 3))
    norm = colors.Normalize(vmin=-1., vmax=1.)
    norm.autoscale(pixel_colors)
    pixel_colors = norm(pixel_colors).tolist()

    hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV)

    h, s, v = cv2.split(hsv)
    fig = plt.figure()
    axis = fig.add_subplot(1, 1, 1, projection="3d")

    axis.scatter(h.flatten(), s.flatten(), v.flatten(), facecolors=pixel_colors, marker=".")
    axis.set_xlabel("Hue")
    axis.set_ylabel("Saturation")
    axis.set_zlabel("Value")
    plt.show()


def plot_rgb_img(rgb):
    pixel_colors = rgb.reshape((np.shape(rgb)[0] * np.shape(rgb)[1], 3))
    norm = colors.Normalize(vmin=-1., vmax=1.)
    norm.autoscale(pixel_colors)
    pixel_colors = norm(pixel_colors).tolist()

    r, g, b = cv2.split(rgb)
    fig = plt.figure()
    axis = fig.add_subplot(1, 1, 1, projection="3d")

    axis.scatter(r.flatten(), g.flatten(), b.flatten(), facecolors=pixel_colors, marker=".")
    axis.set_xlabel("Red")
    axis.set_ylabel("Green")
    axis.set_zlabel("Blue")
    plt.show()


if __name__ == "__main__":
    create_trackbars()
    main()